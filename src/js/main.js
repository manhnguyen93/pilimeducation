import { testing } from './utils/test'
import $ from 'jquery'

$(function () {
  $.extend({
    INIT: {
      MOCK_DATA: 'https://retoolapi.dev/R5lasj/data',

      getImportFunction: () => {
        testing()
      },

      fetchMockAPI: () => {
        $.ajax({
          url: $.INIT.MOCK_DATA,
          type: 'GET',
          contentType: 'application/json',
          dataType: 'json',
          cache: false,
          success: function (json) {
            console.log(json)
          },
          error: function () {
            console.log('Error get API!!')
          },
        })
      },
    },
  })

  $.INIT.getImportFunction()
  $.INIT.fetchMockAPI()
})
